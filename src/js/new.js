import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import SmoothScroll from 'smooth-scroll';

import ConfettiGenerator from "confetti-js";
// import formhand from './form-submission-handler';

window.$ = window.jQuery = window.jquery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;

function getFormData(form) {
  var elements = form.elements;
  var honeypot;

  var fields = Object.keys(elements).filter(function(k) {
    if (elements[k].name === "honeypot") {
      honeypot = elements[k].value;
      return false;
    }
    return true;
  }).map(function(k) {
    if (elements[k].name !== undefined) {
      return elements[k].name;
      // special case for Edge's html collection
    } else if (elements[k].length > 0) {
      return elements[k].item(0).name;
    }
  }).filter(function(item, pos, self) {
    return self.indexOf(item) == pos && item;
  });

  var formData = {};
  fields.forEach(function(name) {
    var element = elements[name];

    // singular form elements just have one value
    formData[name] = element.value;

    // when our element has multiple items, get their values
    if (element.length) {
      var data = [];
      for (var i = 0; i < element.length; i++) {
        var item = element.item(i);
        if (item.checked || item.selected) {
          data.push(item.value);
        }
      }
      formData[name] = data.join(', ');
    }
  });

  // add form-specific values into the data
  formData.formDataNameOrder = JSON.stringify(fields);
  formData.formGoogleSheetName = form.dataset.sheet || "responses"; // default sheet name
  formData.formGoogleSend = form.dataset.email || ""; // no email by default

  return { data: formData, honeypot: honeypot };
}

function handleFormSubmit(event) { // handles form submit without any jquery
  event.preventDefault(); // we are submitting via xhr below
  var form = event.target;
  var formData = getFormData(form);
  var data = formData.data;

  // If a honeypot field is filled, assume it was done so by a spam bot.
  if (formData.honeypot) {
    return false;
  }

  // disableAllButtons(form);
  var url = form.action;
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  // xhr.withCredentials = true;
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      form.reset();
      alert("Thank you for submitting your response we will get back to you shortly");
    }
  };
  // url encode form data for sending as post data
  var encoded = Object.keys(data).map(function(k) {
    return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
  }).join('&');
  xhr.send(encoded);
}

function loaded() {
  //alert('dd');
  // bind to the submit event of our form
  var forms = document.querySelectorAll("form.gform");
  for (var i = 0; i < forms.length; i++) {
    // alert(i);
    forms[i].addEventListener("submit", handleFormSubmit, false);
  }
};


function disableAllButtons(form) {
  var buttons = form.querySelectorAll("button");
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].disabled = true;
  }
}

$(window).scroll(function(event) {
  var scroll = $(window).scrollTop();
  var total = window.innerHeight
  var angle = (scroll / total) * 360;
  $('.flip-box').css('transform', `rotateX(${angle}deg)`);
});

var open = $('.slide')[0];

var confettiSettings = {
  "target": "confetti-holder",
  "max": "80",
  "size": "1",
  "animate": true,
  "props": ["circle", "square", "triangle", "line",
    { "type": "svg", "src": "baloon-orange.svg", "size": 70, "weight": 0.2 },
    { "type": "svg", "src": "baloon-blue.svg", "size": 70, "weight": 0.2 },
    { "type": "svg", "src": "baloon-red.svg", "size": 70, "weight": 0.2 }
  ],
  "colors": [
    [165, 104, 246],
    [230, 61, 135],
    [0, 199, 228],
    [253, 214, 126]
  ],
  "clock": "10",
  "rotate": true,
  "width": "1440",
  "height": "798"
};

var confetti = new ConfettiGenerator(confettiSettings);
confetti.render();

$('.slide').on('click', function() {
  try {
    var html = $(this).children('.detail')[0].innerHTML
    if (html) {
      $('.grid').addClass('open');
      $('.detail-box').hide();
      $('.detail-box .contents').html(html);
      $('.detail-box').fadeIn();
      $('.slide').css('opacity', '.4');
      $(this).css('opacity', '1');
      $(open).removeClass('color');
      $(this).addClass('color');
      open = this;
      if ($(this).attr('class').split(" ").includes("rotate")) {

        loaded();

      }

      //loaded();
    }
  } catch (e) {
    $('.grid').removeClass('open');
    $('.detail-box').hide();
    $('.slide').css('opacity', '1');
    $(open).removeClass('color');
  }
});

$('.detail-box .close').on('click', function() {
  $('.grid').removeClass('open');
  $('.detail-box').hide();
  $('.slide').css('opacity', '1');
  $(open).removeClass('color');
})

// $('.confitte').on('click', function() {
//   $('#confetti-holder').show();
// });

$('#confetti-holder').on('click', function() {
  $(this).hide();
});

$('.confitte').hover(function() {
  $('#confetti-holder').fadeIn();
}, function() {
  $('#confetti-holder').fadeOut();
})